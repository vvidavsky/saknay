//
//  Constants.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation
import SwiftUI

class Constants {
    public static let FIRE_S_POSTS_BY_USER = "posts_by_user";
    public static let FIRE_S_POSTS_RUS = "posts_russia";
    
    public static let APP_MAIN_BLUE = "3c9cff";
    public static let APP_MAIN_BLUE_UIC = UIColor(red:0.09, green:0.52, blue:1.00, alpha:1.0)
    public static let APP_LIGHT_GRAY = "dadada";
    public static let APP_DARK_GRAY = "acacac";
    public static let APP_BALOON_BLUE = "c8ecff";
    public static let APP_BALOON_BLUE_BRD = "acdcff";
    
    public static let CABINET_LINKS = [
        CabItem(name: "Мои Объявления", id: "my_posts"),
        CabItem(name: "Создать новое", id: "create_new"),
        CabItem(name: "Правила", id: "rules"),
        CabItem(name: "Обратная связь", id: "contant_us"),
        CabItem(name: "Выход", id: "sign_out")
    ];
    
    public static let VIEW_STONES = "stones";
    public static let VIEW_JEWELS = "jewels";
    public static let VIEW_WATCH = "watch";
}
