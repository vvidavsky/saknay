//
//  ComponentsModifiers.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 18/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

extension Text {
    func blueBaloon() -> some View {
        self.modifier(Baloon(backgroundColor: Constants.APP_BALOON_BLUE, textColor: Constants.APP_MAIN_BLUE, borderColor: Constants.APP_BALOON_BLUE_BRD))
    }
}

extension View {
    func saknayFormField() -> some View {
        self.modifier(CustomFormField())
    }
}

extension Text {
    func saknayMainTab(isSelected: Bool) -> some View {
        self.modifier(MainTabsStyle(isSelected: isSelected))
    }
}

extension View {
    func postRowStyle() -> some View {
        self.modifier(ListRowStyle())
    }
}

/**
 Модифаер который получаеет параметры с цветами и создает по ним текстовый baloon
 Vlad. 18/02/20
 */
struct Baloon: ViewModifier {
    let backgroundColor: String
    let textColor: String
    let borderColor: String
    
    func body(content: Content) -> some View {
        content
            .padding(15)
            .cornerRadius(15)
            .frame(minWidth: nil, maxWidth: .infinity)
            .background(Color.init(hex: backgroundColor))
            .foregroundColor(Color.init(hex: textColor))
            .font(.system(size: 13))
    }
}

/**
 Модифаер для текстового инпута в приложении, все инпуты используют стили из этого модифаера
 Влад. 18/02/20
 */
struct CustomFormField: ViewModifier {
    func body(content: Content) -> some View {
        content
            .textFieldStyle(RoundedBorderTextFieldStyle())
            .foregroundColor(Color.gray)
            .accentColor(Color.gray)
            .font(.system(size: 13))
    }
}

/**
 Модифаер содающий стили для всех кнопок приложения
 Влад. 20/02/20
 */
struct SaknayButton: ButtonStyle {
    let btnColor: String
    let prsColor: String
    let textColor: Color
    let isDisabled: Bool
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(10)
            .foregroundColor(textColor)
            .background(configuration.isPressed ? Color.init(hex: prsColor) : isDisabled ? Color.init(hex: Constants.APP_LIGHT_GRAY) : Color.init(hex: btnColor))
            .cornerRadius(5)
      }

}

/**
 Модифаер задающий стили главным табам аппликации
 Влад. 06/03/20
 */
struct MainTabsStyle: ViewModifier {
    var isSelected: Bool
    
    func body(content: Content) -> some View {
        content
            .padding(EdgeInsets(top: 10, leading: 20, bottom: 10, trailing: 20))
            .background(self.isSelected ? Color.blue : Color.white)
            .foregroundColor(self.isSelected ? Color.white : nil)
            .cornerRadius(25)
    }

}

struct ListRowStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding(5)
            .frame(maxWidth: .infinity)
            .background(Color.white)
            .cornerRadius(10)
            .padding(2)
            .shadow(radius: 5)
    }
}

