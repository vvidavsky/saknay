//
//  ContentView.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var viewRouter: ViewRouter
    @State var menuOpen: Bool = false
    
    var body: some View {
        ZStack {
            Color.init(hex: Constants.APP_MAIN_BLUE).edgesIgnoringSafeArea(.all)
            VStack(alignment: .leading) {
                HStack {
                    Button(action: {
                        self.openMenu()
                    }, label: {
                        Image("menu").renderingMode(.original)
                    }).padding(10)
                    
                    HStack {
                        Button(action: {self.viewRouter.currentPage = Constants.VIEW_STONES}) {
                            Text("stones").saknayMainTab(isSelected: viewRouter.currentPage == Constants.VIEW_STONES)
                        }
                        
                        Button(action: {self.viewRouter.currentPage = Constants.VIEW_JEWELS}) {
                            Text("jewls").saknayMainTab(isSelected: viewRouter.currentPage == Constants.VIEW_JEWELS)
                        }
                        
                        Button(action: {self.viewRouter.currentPage = Constants.VIEW_WATCH}) {
                            Text("watches").saknayMainTab(isSelected: viewRouter.currentPage == Constants.VIEW_WATCH)
                        }
                    }.frame(maxWidth: .infinity)
                }
                HStack {
                    if viewRouter.currentPage == Constants.VIEW_STONES {
                        StonesView()
                    }
                    if viewRouter.currentPage == Constants.VIEW_JEWELS {
                        JewelsView()
                    }
                    if viewRouter.currentPage == Constants.VIEW_WATCH {
                        WatchesView()
                    }
                }
                Spacer()
            }
            
            SideMenu(width: 270,
                     isOpen: self.menuOpen,
                     menuClose: self.openMenu)
        }
    }
    
    func openMenu() {
        self.menuOpen.toggle()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewRouter: ViewRouter())
    }
}

