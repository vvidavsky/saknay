//
//  InputFieldStruct.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 10/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation

struct InputStruct {
    let text: String
    let model: String
}
