//
//  CabinetItemStruct.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation

struct CabItem {
    let name: String
    let id: String
}
