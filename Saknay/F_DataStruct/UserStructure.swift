//
//  UserStructure.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation

struct User {
    let account_name: String?
    let account_email: String?
    let account_id: String?
    let is_blocked: Bool?
    
    init?(dictionary: [String: Any]) {
        self.account_name = (dictionary["account_name"] as! String)
        self.account_email = (dictionary["account_email"] as! String)
        self.account_id = (dictionary["account_id"] as! String)
        self.is_blocked = (dictionary["is_blocked"] as! NSNumber == 0 ? false : true)
    }
}
