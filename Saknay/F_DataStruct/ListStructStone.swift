//
//  ListStructStone.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 06/03/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation
import SwiftUI

struct StoneStructure: Identifiable {
    var id = UUID()
    var thumbnail: String
    var product: String
    var stone_shape: Any?
    var stone_weight: Any?
    var stone_color: Any?
    var stone_clarity: Any?
    var stone_origin: Any?
    var certificate: Any?
    var city: String
    var weight_category: String
}



//class StoneStructure: ObservedObject<ObjectType: ObservableObject> {
//    @Published var thumbnail: String
//    @Published var product: String
//    @Published var stone_shape: String?
//    @Published var stone_weight: Any?
//    @Published var stone_color: Any?
//    @Published var stone_clarity: Any?
//    @Published var stone_origin: Any?
//    @Published var certificate: Any?
//    @Published var city: String
//    @Published var weight_category: Any?
//    
//    init(thumb: String, prod: String, stone_sh: String, stone_wei: Any, stone_col: Any, stone_clar: Any, stone_orig: Any, certif: Any, cit: String, weight_cat: Any) {
//        thumbnail = thumb
//        product = prod
//        stone_shape = stone_sh
//        stone_weight = stone_wei
//        stone_color = stone_col
//        stone_clarity = stone_clar
//        stone_origin = stone_orig
//        certificate = certif
//        city = cit
//        weight_category = weight_cat
//    }
//}

