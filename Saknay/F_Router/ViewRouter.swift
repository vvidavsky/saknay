//
//  ViewRouter.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 03/03/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation
import Combine
import SwiftUI

class ViewRouter: ObservableObject {
    let objectWillChange = PassthroughSubject<ViewRouter,Never>()
    var currentPage: String = Constants.VIEW_STONES {
        didSet {
            objectWillChange.send(self)
        }
    }
}
