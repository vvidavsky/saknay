//
//  FormSignUp.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 18/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct SignUpForm: View {
    @State private var nick: String = ""
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var passwordRep: String = ""
    @State private var showLoader: Bool = false
    
    private var hasEmptyFields: Bool {
        nick.isEmpty || email.isEmpty || password.isEmpty || passwordRep.isEmpty
    }
    
    private var passwordNotEqual: Bool {
        password != passwordRep
    }
    
    private var passwordShort: Bool {
        password.count < 6
    }
    
    private func createUser() {
        self.showLoader = true
        FireAuth.createUser(email: self.email, password: self.password) { result in
            if case .success(let authData) = result {
                self.showLoader = false
                print("\(authData)")
            } else if case .failure(let error) = result {
                self.showLoader = false
                print("\(error)")
            }
        }
    }
    
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                Text("signup").foregroundColor(Color.init(hex: Constants.APP_MAIN_BLUE))
                Divider()
                Text("nick").font(.system(size: 13)).foregroundColor(Color.gray)
                TextField("", text: $nick).saknayFormField()
                Text("email").font(.system(size: 13)).foregroundColor(Color.gray).padding(.top, 15)
                TextField("", text: $email).saknayFormField()
                Text("password").font(.system(size: 13)).foregroundColor(Color.gray).padding(.top, 15)
                SecureField("minPassLength", text: $password).saknayFormField()
                Text("passRepeat").font(.system(size: 13)).foregroundColor(Color.gray).padding(.top, 15)
                SecureField("minPassLength", text: $passwordRep).saknayFormField()
            }
            Spacer()
            VStack {
                if self.showLoader {
                    Preloader()
                } else {
                    Button(action: createUser) {
                        Text("signup")
                    }
                    .disabled(hasEmptyFields || passwordNotEqual || passwordShort)
                    .buttonStyle(SaknayButton(btnColor: Constants.APP_MAIN_BLUE,
                                              prsColor: Constants.APP_BALOON_BLUE,
                                              textColor: Color.white,
                                              isDisabled: hasEmptyFields || passwordNotEqual || passwordShort))
                }
            }.frame(width: nil, height: 50)
        }.padding(20).background(Color.white)
    }
}
