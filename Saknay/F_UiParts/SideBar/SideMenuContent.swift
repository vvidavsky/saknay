//
//  SideMenuContent.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI
import Firebase

struct MenuContent: View {
    @State var loggedInUser: Bool = false
    
    var body: some View {
        VStack {
                if loggedInUser {
                    LoggedInView()
                } else {
                    NotLoggedInView()
                }
            }.onAppear(perform: monitorLoggedIn)
    }
    
    private func monitorLoggedIn() {
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil {
                self.loggedInUser = true
            } else {
                self.loggedInUser = false
            }
        }
    }
}
