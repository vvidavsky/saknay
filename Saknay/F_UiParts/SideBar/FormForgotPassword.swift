//
//  FormForgotPassword.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 18/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct ForgotPasswordForm: View {
    @State private var email: String = ""
    @State private var alertTitle: String = ""
    @State private var alertText: String = ""
    @State private var showLoader: Bool = false
    @State private var showViewAlert: Bool = false
    
    
    func reesetPassword() {
        self.showLoader = true
        FireAuth.resetPassword(email: self.email) { result in
            self.showLoader = false
            if case .success(_) = result {
                self.alertTitle = ""
                self.alertText = NSLocalizedString("fireResetSent", comment: "")
                self.showViewAlert = true
            } else if case .failure(let error) = result {
                self.alertTitle = NSLocalizedString("error", comment: "")
                self.alertText = FireAuth.handleError(error: error)
                self.showViewAlert = true
            }
        }
    }
    
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                Text("forgot").foregroundColor(Color.init(hex: Constants.APP_MAIN_BLUE))
                Divider()
                Text("resetPwMsg").blueBaloon()
                Text("email").font(.system(size: 13)).foregroundColor(Color.gray).padding(.top, 15)
                TextField("", text: $email).saknayFormField()
            }
            Spacer()
            VStack {
                if self.showLoader {
                    Preloader()
                }  else {
                    Button(action: reesetPassword) {
                        Text("resetPasswordBtn")
                    }
                    .disabled(email.isEmpty)
                    .buttonStyle(SaknayButton(btnColor: Constants.APP_MAIN_BLUE,
                                              prsColor: Constants.APP_BALOON_BLUE,
                                              textColor: Color.white,
                                              isDisabled: email.isEmpty))
                }
            }.frame(width: nil, height: 50)
        
        }.padding(20).background(Color.white)
            .alert(isPresented: $showViewAlert) {
                Alert(title: Text(self.alertTitle), message: Text(self.alertText), dismissButton: .default(Text("close")))
        }
    }
}
