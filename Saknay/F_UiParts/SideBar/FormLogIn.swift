//
//  FormLogIn.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct LoginForm: View {
    let fields = [InputStruct(text: "e-mail", model: "email"), InputStruct(text: "password", model: "password")]
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var alertTitle: String = ""
    @State private var alertText: String = ""
    @State private var showLoader: Bool = false
    @State private var showViewAlert: Bool = false
    
    private var loginDisabled: Bool {
        email.isEmpty || password.isEmpty
    }
    
    private func signIn() {
        self.showLoader = true
        FireAuth.signIn(email: self.email, password: self.password) { result in
            self.showLoader = false
            if case .success(let authData) = result {
                print("\(authData)")
            } else if case .failure(let error) = result {
                print(error)
               self.alertTitle = NSLocalizedString("error", comment: "")
               self.alertText = FireAuth.handleError(error: error)
               self.showViewAlert = true
            }
        }
    }
    
    var body: some View {
        VStack {
            VStack(alignment: .leading) {
                Text("authent").foregroundColor(Color.init(hex: Constants.APP_MAIN_BLUE))
                Divider()
                Text("email").font(.system(size: 13)).foregroundColor(Color.gray)
                TextField("", text: $email).saknayFormField()
                Text("Пароль").font(.system(size: 13)).foregroundColor(Color.gray).padding(.top, 20)
                SecureField("", text: $password).saknayFormField()
            }
            Spacer()
            VStack {
                if self.showLoader {
                    Preloader()
                }  else {
                    Button(action: signIn) {
                        Text("login")
                    }
                    .disabled(loginDisabled)
                    .buttonStyle(SaknayButton(btnColor: Constants.APP_MAIN_BLUE,
                                              prsColor: Constants.APP_BALOON_BLUE,
                                              textColor: Color.white,
                                              isDisabled: loginDisabled))
                }
            }.frame(width: nil, height: 50)
        }.padding(20).background(Color.white).alert(isPresented: $showViewAlert) {
                Alert(title: Text(self.alertTitle), message: Text(self.alertText), dismissButton: .default(Text("close")))
        }
    }
}
