//
//  NotLoggedInView.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct NotLoggedInView: View {
    
//    init() {
//        UITabBar.appearance().barTintColor = Constants.APP_MAIN_BLUE_UIC;
//    }
    
    var body: some View {
        TabView {
            LoginForm().tabItem { getLabel(text: "login", icon: "login") }.tag(1)
            SignUpForm().tabItem { getLabel(text: "signup", icon: "login")}.tag(2)
            ForgotPasswordForm().tabItem {getLabel(text: "forgot", icon: "login")}.tag(3)
        }.accentColor(Color.init(hex: Constants.APP_MAIN_BLUE))
    }
    

    func getLabel(text: String, icon: String) -> some View {
        let str = NSLocalizedString(text, comment: "");
        return VStack() {
            Image(icon)
            Text(str)
        }
    }
}
