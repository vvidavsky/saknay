//
//  LoggedInView.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct LoggedInView: View {
    private let items = Constants.CABINET_LINKS
    var body: some View {
        VStack {
                ForEach(0 ..< items.count) {
                    i in
                    Button(action: {self.goToCabinetView(id: self.items[i].id)},
                           label: {
                        Text(self.items[i].name)
                        Spacer()
                    })
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .frame(height: 50)
                        .background(Color.green)
                        .padding()
                }
        }
    }
    
    private func goToCabinetView(id: String) {
        switch id {
        case "sign_out":
            FireAuth.signOut();
        default:
            print("huy")
        }
    }
}
