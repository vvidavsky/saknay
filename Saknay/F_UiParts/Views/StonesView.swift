//
//  StonesView.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 03/03/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct StonesView: View {
    @State var postsToShow: [StoneStructure] = []
    
    init () {
        UITableView.appearance().separatorStyle = .none
        UITableView.appearance().backgroundColor = UIColor.clear
        UITableView.appearance().tableFooterView = UIView()
    }
    
    private func getStonesPosts() {
        let this = self
        getPosts(uri: "\(Constants.FIRE_S_POSTS_RUS)/slim/stone", type: Constants.VIEW_STONES) { posts in
            if case .success(let stonesPosts) = posts {
                this.postsToShow = stonesPosts as! [StoneStructure]
                print("\(stonesPosts)")
            } else if case .failure(let error) = posts {
                print("\(error)")
            }
        }
    }
    
    var body: some View {
        VStack {
            if self.postsToShow.count > 0 {
                List {
                    ForEach(self.postsToShow) {post in
                        VStack {
                            HStack {
                                AsyncImage(url: URL(string: "\(post.thumbnail)")!, placeholder: Text("Huy..."))
                                    .frame(minHeight: 100, maxHeight: 100)
                                    .frame(minWidth: nil, maxWidth: 150)
                                    .aspectRatio(contentMode: .fit).cornerRadius(10)
                                VStack {
                                    Text("\(post.id)").foregroundColor(Color.red)
                                    Text("\(post.city)").foregroundColor(Color.red)
                                }.frame(maxWidth: .infinity)
                                }.postRowStyle()
                        }.listRowBackground(Color.init(hex: Constants.APP_MAIN_BLUE))
                    }
                }
            }
        }.onAppear{
            self.getStonesPosts()
        }
    }
}
