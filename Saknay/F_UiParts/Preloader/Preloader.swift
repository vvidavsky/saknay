//
//  Preloader.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 10/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import SwiftUI

struct Preloader: View {
    @State private var animateTrimmedCicle = false
    @State private var animateInnerCicle = true
    var body: some View {
        ZStack {
            Color.white.edgesIgnoringSafeArea(.all)
            Circle()
                .frame(width: 30, height: 30)
                .foregroundColor(Color.init(hex: Constants.APP_MAIN_BLUE))
                .scaleEffect(animateInnerCicle ? 1 : 0.5)
                .animation(Animation.interpolatingSpring(stiffness: 170, damping: 20).speed(1.0).repeatForever(autoreverses: true))
                 .onAppear() {
                    self.animateInnerCicle.toggle()
            }
            ZStack {
                Circle()
                    .trim(from: 3/4, to: 1)
                    .stroke(style: StrokeStyle(lineWidth: 3, lineCap: .round, lineJoin: .round))
                    .frame(width: 50, height: 50)
                    .foregroundColor(Color.init(hex: Constants.APP_MAIN_BLUE))
                Circle()
                    .trim(from: 3/4, to: 1)
                    .stroke(style: StrokeStyle(lineWidth: 3, lineCap: .round, lineJoin: .round))
                    .frame(width: 50, height: 50)
                    .foregroundColor(Color.init(hex: Constants.APP_MAIN_BLUE))
                    .rotationEffect(.degrees(-180))
            }.scaleEffect(animateTrimmedCicle ? 1 : 0.4 )
                .rotationEffect(.degrees(animateTrimmedCicle ? 360 : 0))
                .animation(Animation.interpolatingSpring(stiffness: 170, damping: 20).speed(1.0).repeatForever(autoreverses: true))
                 .onAppear() {
                self.animateTrimmedCicle.toggle()
            }
        }
    }
}
