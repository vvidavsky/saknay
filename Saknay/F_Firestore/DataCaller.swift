//
//  DataCaller.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 06/03/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation
import FirebaseFirestore

let db = Firestore.firestore()

public func getPosts(uri: String, type: String, completion: @escaping (Result<[Any], Error>) -> Void) {
    return db.collection(uri)
        .whereField("approved", isEqualTo: true)
        .order(by: "renewed_at_ts", descending: true)
        .limit(to: 5).getDocuments() { (querySnapshot, err) in
            if let err = err {
                 completion(.failure(err))
            } else {
                switch type {
                case Constants.VIEW_STONES:
                    var readyData: [StoneStructure] = []
                    for document in querySnapshot!.documents {
                        readyData.append(StoneStructure(thumbnail: document.get("thumbnail") as! String,
                                                        product: document.get("product") as! String ,
                                                        stone_shape: document.get("stone_shape") as? String,
                                                        stone_weight: document.get("stone_weight"),
                                                        stone_color: document.get("stone_color"),
                                                        stone_clarity: document.get("stone_clarity"),
                                                        stone_origin: document.get("stone_origin"),
                                                        certificate: document.get("certificate"),
                                                        city: document.get("city") as! String,
                                                        weight_category: document.get("weight_category") as! String))
                    }
                    completion(.success(readyData))
                default:
                   print("")
                }
                 
            }
    }
}
