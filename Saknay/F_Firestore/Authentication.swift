//
//  Authentication.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation
import Firebase

class FireAuth {
    static func createUser(email: String, password: String, completion: @escaping (Result<Any, Error>) -> Void){
        Auth.auth().createUser(withEmail: email, password: password) {authResult, error in
            if error != nil {
                completion(.failure(error!))
            } else {
                completion(.success(authResult!))
            }
        }
    }
    
    static func signIn(email: String, password: String, completion: @escaping (Result<Any, Error>) -> Void){
        Auth.auth().signIn(withEmail: email, password: password) {authResult, error in
            if error != nil {
                completion(.failure(error!))
            } else {
                completion(.success(authResult!))
            }
        }
    }
    
    static func signOut() {
        try! Auth.auth().signOut()
    }
    
    static func resetPassword(email: String, completion: @escaping (Result<Any, Error>) -> Void){
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if error != nil {
                completion(.failure(error!))
            } else {
                completion(.success(true))
            }
        }
    }
    
    
    static func handleError(error: Error) -> String {
        let errCode = AuthErrorCode(rawValue: error._code)
        var str: String = ""
        switch errCode {
        case .invalidEmail:
            str = NSLocalizedString("errFireWrongEmail", comment: "")
        case .userNotFound:
            str = NSLocalizedString("errFireUserNotFound", comment: "")
        case .wrongPassword:
            str = NSLocalizedString("errFireWrongPasswd", comment: "")
        case .emailAlreadyInUse:
            str = NSLocalizedString("errFireEmailExists", comment: "")
        default:
            str = ""
        }
        
        return str
    }
}
