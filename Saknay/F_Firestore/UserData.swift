//
//  UserData.swift
//  Saknay
//
//  Created by Vlad Vidavsky on 07/02/2020.
//  Copyright © 2020 Vlad Vidavsky. All rights reserved.
//

import Foundation
import FirebaseFirestore

class UserData {
    let db = Firestore.firestore()
    var userData: Any?
    
    func SetUserData(user: String) {
        if user != "" {
            let userRef = db.document("users/\(user)")
            userRef.getDocument { (document, error) in
                if document!.exists {
                    self.userData = User(dictionary: document!.data()!)
                    print(self.userData!)
                }
            }
        } else {
            self.userData = nil
        }
    }
    
    func getUserData() -> Any {
        return userData!
    }
}
